# Weather app screenshots

![Home page](./1-home.png "Home Page")

![Search page](./2-search-result.png "Search result page")

![Detail page](./3-weather-detail-page.png "Detail page")