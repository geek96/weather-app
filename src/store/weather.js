import Vue from 'vue'
import  weather from '@/api/weather'

export default {
  namespaced: true,
  state: {
    locations: null,
    error: null
  },
  mutations: {
    setLocations(state, locations) {
      state.locations = locations
    },
    updateLocations(state, location) {
      const updatedItems = state.locations.map(l => {
        if (l.title === location.title)
          l = location
        return l
      })
      Vue.set(state, 'locations', updatedItems)
    },
    setError(state, error) {
      state.error = error
    }
  },
  actions: {
    query(ctx, keyword) {
      return weather.query(keyword)
    },
    async getById(ctx, woeid) {
      return await weather.getById(woeid)
    },
    async search({commit, dispatch}, keyword) {
      commit('setLocations', null)
      commit('setError', null)

      try {
        const response =  await dispatch('query', keyword)
        commit('setLocations', response)
      } catch (err) {
        commit('setError', err.message)
      }
    },
    async fetchDefault({ commit, dispatch }, keyword) {
      try {
        const response =  await dispatch('query', keyword)
        commit('updateLocations', response.shift())
      } catch (err) {
        commit('setError', err.message)
      }
    },
    async fetchDetail({commit, dispatch}, woeid) {
      try {
        const data = await dispatch('getById', woeid)
        commit('updateLocations', data)
      } catch(err) {
        commit('setError', err)
      }
    }
  }
}