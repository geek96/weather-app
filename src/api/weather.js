import axios from "axios"

const baseURL = 'http://localhost/weather.php'

const searchUrl =  (keyword) => {
  return `${baseURL}?command=search&keyword=${keyword}`
}
const locationUrl = (woeid) => {
  return `${baseURL}?command=location&woeid=${woeid}`
}

const errorHandler = (e) => {
  const error = new Error(e)
  error.message = e.response.data
  throw error
}

export default {
  query(keyword) {
    return axios.get(searchUrl(keyword)).then(r => r.data).catch(errorHandler)
  },
  getById(woeid) {
    return axios.get(locationUrl(woeid)).then(r => r.data).catch(errorHandler)
  }
}